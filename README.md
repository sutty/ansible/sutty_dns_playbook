Sutty DNS Playbook
==================

DNS Playbook for Sutty.

Features
--------

### Decentralized and uncoordinated nameserver network

Each host is a self-referenced nameserver.  This means it will only
publish its own IP addresses for the same records.

Since DNS resolvers pick a NS at random and retry when one is
unresponsive, we can load-balance and reply with a working address
without a coordination system.

When a node goes down, it takes down its information with it.

### Container DNS and "service mesh"

Each node is a dynamic DNS zone for its own containers.  If you want to
reach a specific container, it will have a container_name.node.org
address.

Combined with the Ekumen[^1] containers in different nodes can talk to
each other.

[^1]: TODO: Add link here! Temporary address:
  <https://0xacab.org/pip/rap>

They achieve this by self-registering their IPv6 addresses as AAAA
records.  Since the IPv6 subnet is shared between nodes, thanks to the
Ekumen, it's possible to resolve their addresses using their regular DNS
resolvers.

Inventory
---------

Create, link or clone an `inventory.yml` file with the host roles.

```yaml
---
sutty_panel:
  hosts:
    host.name:
    host2.name:
sutty_nodes:
  hosts:
    node.name:
sutty_domains:
  hosts:
    anoth.er:
    cust.om:
    doma.in:
```

`sutty_domains` is a special inventory group for custom domains hosted
at Sutty's nameservers.

You can defined custom DNS records on `group_vars/sutty_domains.yml`
or `host_vars/anoth.er.yml` files.

Please take note that Ansible doesn't deep merge these variables, so if
you set custom records for a single domain but want to keep custom
records from the whole group, you need to copy them from the group to
the host file.

E-mail is auto-configured for every domain.  If you want to disable it
you can set `email: false`, and optionally provide your own e-mail
records (MX, SPF, DMARC, DKIM).

```yaml
---
sutty_dns:
  email: false
  extra_records:
  - name: "api"
    comment: "API"
    records:
    - type: "CNAME"
      value: "api.sutty.nl."
  - name: "@"
    comment: "External e-mail"
    records:
    - type: "MX"
      value: "0 smtp.riseup.net."
```

Host and group vars
-------------------

Create, link or clone a directory `host_vars/` with a YAML file for each
host, containing all host-specific variables.

A default `group_vars/` directory exists.  Check CHANGEME variables for
stuff that requires customization.

Credentials
-----------

Auto-generated credentials are stored on `_credentials/` and ignored by
git.
